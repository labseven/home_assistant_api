// index.js
const getJSON =require("./get_json");

const express = require("express");
const http = require('http');
const moment = require('moment')
require('dotenv').config()


const app = express();
// Specify location of views
app.set("views", "./server/views");

// Start up the server on port 3000
const port = 3000;
const ha_config = {
  "host": process.env.ha_host,
  "port": process.env.ha_port,
  "auth_header": process.env.ha_auth_header
}

const options_get = {
  host: ha_config.host,
  port: ha_config.port,
  path: '/api/states/switch.coffee_machine',
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ha_config.auth_header}`
  }
};

const options_post = {
  host: ha_config.host,
  port: ha_config.port,
  path: '/api/services/script/blink_office_lamp',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ha_config.auth_header}`
  }
};

async function getHA() {
  output = await getJSON.getJSON(options_get, (_, r) => console.log(r))
}

getHA()

app.get("/", (req, res) => {res.send("hi, thanks for stopping by")})
app.get('/coffee', (req, res) => {
  getJSON.getJSON(options_get, (_, r) => {
    res.send(`<p>Coffee machine is ${r.state}. (${prettyTime(r.last_changed)} ago)</p>`)
  })
})
app.post('/coffee', (req, res) => {
  res.send(501, "not ready yet")
})

app.post('/office_lights', (req, res) => {
  console.log("blinking lights", req);
  getJSON.postJSON(options_post, (status, r) => {
    console.log(r);
    res.status(status).send("");
  })
})

app.listen(port, () => console.log(`Server running on http://localhost:3000`));


function prettyTime(date) {
  diff = moment().diff(moment(date))
  duration = moment.duration(diff)

  secs = diff / 1000

  if (secs < 60) { return `${duration.seconds()} seconds`}
  if (secs < 60 * 60) { return `${duration.minutes()} minutes`}
  if (secs < 60 * 60 * 24) { return `${duration.hours()} hours`}
  return `${duration.days()} days`
}
