# home_assistant_api

Expose status and even control of you home assistant entites.

### Running
create a `.env` file with the following values:
```
ha_host="192.168.1.10"
ha_port="8123"
ha_auth_header="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaHELPMEIMSTUCKINANENTROPYMACHINEaaaaaaaaaa"
```
then run:
`docker-compose up -d`
